import torch
import numpy as np
from melgan import MelVocoder
from librosa.output import write_wav
import argparse
import os
import json
import sys
from pathlib import Path

curr_emotion = ['Angry', 'Happy', 'Disgust', 'Sleep']

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--emotion", required=True)
    parser.add_argument("--text", required=True)
    parser.add_argument("--filename", required=True)
    parser.add_argument("--output", default = "logs\\")
    
    args = parser.parse_args()
    return args

def tacotron(text):   
    """
    It generate the melspectrogram from the text 
    
    Args:
        text (str): the given sentence from the backend to be transformed
        
    Returns:
        mel (array): numpy array that hold the melspectrogram for the text
    """
    tacotron2 = torch.hub.load('nvidia/DeepLearningExamples:torchhub', 'nvidia_tacotron2') 
    tacotron2 = tacotron2.to('cpu')
    tacotron2.eval()
    # preprocessing
    sequence = np.array(tacotron2.text_to_sequence(text, ['english_cleaners']))[None, :]
    sequence = torch.from_numpy(sequence).to(device='cpu', dtype=torch.int64)
    # run the models
    with torch.no_grad():
        _, mel, _, _ = tacotron2.infer(sequence)
    
    return mel


def melgan(mel, mel_path):
    """
    It generate the audio from the melspectrogram from the model
    
    Args:
        mel2wav_path (str or Path): path to the root folder of pt file model
        mel (array): numpy array that hold the melspectrogram for the text
        
    Returns:
        recons (array): numpy array that represent the audio waves
    """
    vocoder = MelVocoder(mel_path)
    recons = vocoder.inverse(mel).squeeze().cpu().numpy()   
    return recons



def path_handler(emotion):
    """
    It handle the path based on the give emotion, to point to the right model, and generate the output path
    
    Args: 
        emotion: the given emotion from the backend
        
    Returns:
        mel_path: path to the desired model .pt
        out_path: the path to save the audio file generated
    """
    if emotion not in curr_emotion:
        return None
    curdir = os.path.dirname(os.path.realpath(__file__)) 
    mel_path = curdir + '\\'+ str(emotion) 
    out_path = curdir + '\\' + str("logs\\")
        
    return mel_path, out_path
    

def save_wave(out_path, filename, recons):
    """
    Save audio file to the local drive
    
    Args: 
        out_path: the output path to save the audio to
        filename: the filename for the audio
        recons: numpy array that holds the audio
    """
    rate = 22050
    write_wav(out_path +  str(filename) + '.wav', recons, rate)
    

def save_json(recons, filename):
    lists = recons.tolist()
    json_str = json.dumps(lists)
    with open('data.json', 'w', encoding='utf-8') as outfile:
        json.dump(json_str, outfile)

def main():
    emotion = str(sys.argv[1])
    text = str(sys.argv[2])
    filename = str(sys.argv[3])
    mel = tacotron(text)
    mel_path, out_path = path_handler(emotion)
    recons = melgan(mel, mel_path)
    save_wave(out_path, filename, recons)
    #save_json(recons, filename)
    print(out_path)
    print("Done")
    

if __name__ == "__main__":
    main()
    

