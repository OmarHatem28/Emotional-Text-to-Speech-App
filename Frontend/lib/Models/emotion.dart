import 'package:flutter/material.dart';

import '../main.dart';

class Emotion {
  String name;
  String image;
  Emotions emotionType;
  Color color;
  String trainedSound;

  Emotion({
    this.name,
    this.image,
    this.emotionType,
    this.color,
    this.trainedSound,
  });
}
