import '../main.dart';

class Sentence {
  String phrase;
  Emotions emotion;

  Sentence({this.phrase, this.emotion});

  Map<String, String> toJson() {
    String emotionSt;
    switch (emotion) {
      case Emotions.Happy:
        emotionSt = "Happy";
        break;
      case Emotions.Angry:
        emotionSt = "Angry";
        break;
      case Emotions.Disgust:
        emotionSt = "Disgust";
        break;
      case Emotions.Sleep:
        emotionSt = "Sleep";
        break;
      default:
        emotionSt = "Happy";
    }
    return {
      'phrase': phrase.toString(),
      'emotion': emotionSt.toString(),
    };
  }
}
