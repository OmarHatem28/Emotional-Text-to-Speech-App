exports.validatePhrase = (phrase) => {
  //var regex = new RegExp('/^[a-zA-Z]+$/');
  if (/^\w+(\s+\w+)*$/.test(phrase)) {
    return "OK";
  } else {
    return "The phrase contain strange chrachters please Enter English word only";
  }
};
exports.checkEmotion = (emotion, check) => {
  var emotions = ["Angry", "Disgust", "Sleep", "Happy"];
  //console.log(emotions.indexOf(emotion));
  return emotions.indexOf(emotion);
};
