# Inside Out (Emotional_Text_To_Speech)

The problem we are trying to solve is the apathetic and robotic voices generated from regular vanilla text-to-speech (TTS) systems, which limits the possible applications of TTS. Instead, we aim to create a TTS system that generates more humanlike voices that show emotions. This can save time and money that would usually be spent on hiring people and recording their voices for any purpose.

# System Architecture

<img src="./srcs/Onion Layers.png" width="900px" height="600px">

## File Structure
- Front End/ : Flutter application
- Back End/ : Nodejs
- End Api/ : Python script
- <a href="https://github.com/NVIDIA/tacotron2">Tacotron 2</a> 
- <a href="https://github.com/descriptinc/melgan-neurips">MelGAN</a> 

# Details

For more details please refer to the <a href="./srcs/Final year Documentation.pdf">documentation</a>

# Colaborators

- Omar Hatem
- Bassem Mohammed
- Mina Shafik
- Maria George
- Andrew Maher