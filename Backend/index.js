path = require("path");
var fs = require("fs");
ms = require("mediaserver");
var validator = require("./Validator");
var http = require("http");
var app = require("express")();
const bodyParser = require("body-parser");
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
//console.log(__dirname);
let reqPath = path.join(__dirname, "../DeepLearning/main.py");

function createName(req) {
  let today = new Date();
  let filename =
    req.query.emotion +
    today.getFullYear() +
    "-" +
    (today.getMonth() + 1) +
    "-" +
    today.getDate();
  return filename;
}
function validate(req, res) {
  var message = validator.validatePhrase(req.query.phrase);
  if (message != "OK") {
    return res.status(400).json({ success: false, error: message });
  } else if (validator.checkEmotion(req.query.emotion) < 0) {
    return res
      .status(400)
      .json({ success: false, error: `This Emotion ${emotion} Not found` });
  }
}
async function runPythonScript(req, res) {
  let filename = createName(req);
  const { PythonShell } = require("python-shell");
  var options = {
    mode: "text",
    args: [req.query.emotion, req.query.phrase, filename],
  };

  let audioFile;
  PythonShell.run(reqPath, options, function (err, results) {
    if (err) {
      console.log("Andrew error: " + err);
      throw err;
    }
    console.log("results: %j", results);
    if (results[1] === "Done") {
      console.log("Getting file");
      console.log(results[0] + filename);
      audioFile = results[0] + filename + ".wav";
      console.log(audioFile);
      sendAudio(audioFile, res, filename, req);
    }
    return audioFile;
  });
}

function sendAudio(audioFile, res, filename, req) {
  res.download(audioFile);
}

app.get("/myaudio", async function (req, res) {
  //console.log("Entring get method");
  console.log(req.query);
  validate(req, res);
  if (!res.headersSent) {
    try {
      await runPythonScript(req, res);
    } catch (e) {
      return res.status(400).json({ success: false, error: e });
    }
  }
});
var port = process.env.port || 3000;

app.listen(port, function () {
  console.log("server started listnening on port " + port);
});
