import 'dart:async';
import 'dart:math';

import 'package:animator/animator.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:emotional_text_to_speech/Models/Sentence.dart';
import 'package:emotional_text_to_speech/Models/emotion.dart';
import 'package:emotional_text_to_speech/Repository/SentenceRepo.dart';
import 'package:emotional_text_to_speech/Utilities/AlertDialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_visualizers/Visualizers/CircularBarVisualizer.dart';
import 'package:flutter_visualizers/visualizer.dart';

import 'package:emotional_text_to_speech/Utilities/Constants.dart';
import '../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final player = AssetsAudioPlayer.newPlayer();
  Audio currentAudio;

  int selectedIndex;

  bool loading = false;

  List<String> sliderImages = [
    'assets/images/photo1.webp',
    'assets/images/download.jpg',
    'assets/images/download2.jpg',
    'assets/images/joy_wide.jpg',
    'assets/images/anger_wide.jpg',
    'assets/images/disgust_wide.jpg',
    'assets/images/fear_wide.jpg',
    'assets/images/sad.jpg',
  ];

  List<Emotion> emotions = [
    Emotion(
      name: "Happy",
      image: 'assets/images/happy.png',
      emotionType: Emotions.Happy,
      color: Colors.yellow[700],
      trainedSound: "assets/sounds/amused.wav",
    ),
    Emotion(
      name: "Angry",
      image: 'assets/images/anger.png',
      emotionType: Emotions.Angry,
      color: Colors.red,
      trainedSound: "assets/sounds/anger.wav",
    ),
    Emotion(
      name: "Disgust",
      image: 'assets/images/disgust.png',
      emotionType: Emotions.Disgust,
      color: Colors.green,
      trainedSound: "assets/sounds/digust.wav",
    ),
    Emotion(
      name: "Sleep",
      image: 'assets/images/sleep.png',
      emotionType: Emotions.Sleep,
      color: Colors.blue,
      trainedSound: "assets/sounds/sleepy.wav",
    ),
  ];

  TextEditingController textController = TextEditingController();

  Timer timer;

  @override
  void initState() {
    super.initState();
    player.current.listen((playingAudio) {
      if (player.isPlaying.value) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Image.asset(
                  "assets/images/Balls_copy_3.png",
                  fit: BoxFit.fill,
                ),
                player.isPlaying.value ? buildSoundVisualizer() : Container(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: hp(12, context)),
                        Image.asset(
                          "assets/images/Shape_4.png",
                          height: hp(40, context),
                          width: hp(40, context),
                        ),
                        Text(
                          selectedIndex == null
                              ? ""
                              : emotions[selectedIndex].name,
                          style: TextStyle(
                              color: selectedIndex == null
                                  ? Colors.purple
                                  : emotions[selectedIndex].color,
                              fontSize: 24,
                              fontWeight: FontWeight.w700),
                        ),
                        SizedBox(height: hp(2, context)),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: wp(2.5, context)),
                          child: Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    Constants.BorderRadius)),
                            child: TextFormField(
                              controller: textController,
                              decoration: InputDecoration(
                                  hintText: "Enter your text",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          Constants.BorderRadius))),
                            ),
                          ),
                        ),
                        SizedBox(height: hp(2, context)),
                        Container(
                          height: hp(10, context),
                          padding:
                              EdgeInsets.symmetric(horizontal: wp(10, context)),
                          child: ListView.builder(
                            itemCount: emotions.length,
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, i) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: buildAnimatedCard(emotions[i], i),
                              );
                            },
                          ),
                        ),
                        SizedBox(height: hp(2, context)),
                        buildGenerateButton(),
                      ],
                    ),
                  ),
                ),
                selectedIndex == null
                    ? Container()
                    : Positioned(
                        top: hp(25, context),
                        left: wp(37, context),
                        child:
                            loading ? buildLoading() : buildPlayPauseButton()),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildGenerateButton() {
    return InkWell(
      onTap: generateSound,
      borderRadius: BorderRadius.circular(Constants.BorderRadius),
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: wp(10, context), vertical: hp(2, context)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Constants.BorderRadius),
            color: selectedIndex == null
                ? Colors.purple
                : emotions[selectedIndex].color),
        child: Text(
          "Generate".toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  void generateSound() async {
    if (selectedIndex == null) {
      showWarningDialog(
        "Note",
        "Please Select an emotion first.",
        context,
      );
      return;
    } else if (textController.text.isEmpty) {
      showWarningDialog(
        "Note",
        "Please Enter your text first.",
        context,
      );
      return;
    }
    setState(() {
      loading = true;
    });
    SentenceRepo sentenceRepo = new SentenceRepo();
    var response = await sentenceRepo.sendSentence(
      Sentence(
          phrase: textController.text,
          emotion: emotions[selectedIndex].emotionType),
    );
    currentAudio = Audio.file(response);
    setState(() {
      loading = false;
    });
  }

  Widget buildLoading() {
    return Padding(
      padding: EdgeInsets.only(left: wp(8, context), top: hp(5, context)),
      child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(
          Colors.white,
        ),
      ),
    );
  }

  Widget buildPlayPauseButton() {
    return GestureDetector(
      onTap: () async {
        setState(() {
          loading = true;
        });
        try {
          if (player.isPlaying.value) {
            await player.pause();
            timer?.cancel();
          } else {
            await player.open(currentAudio);

            timer = Timer.periodic(Duration(milliseconds: 200), (tick) {
              setState(() {});
            });
          }
        } catch (t) {
          showWarningDialog(
              "Note", "Something went wrong, Please try again later!", context);
          timer?.cancel();
        }
        setState(() {
          loading = false;
        });
      },
      child: Icon(
        player.isPlaying.value ? Icons.pause : Icons.play_arrow,
        color: Colors.white,
        size: 100,
      ),
    );
  }

  Widget buildAnimatedCard(Emotion emotion, int i) {
    return Animator(
      tween: Tween<Offset>(begin: Offset(-0.9, 1), end: Offset(0, 0)),
      duration: Duration(milliseconds: (i + 4) * 1200),
      curve: Curves.elasticOut,
      repeats: 1,
      builder: (context, animatorState, child) {
        return FractionalTranslation(
          translation: animatorState.value,
          child: GestureDetector(
            onTap: () {
              setState(() {
                selectedIndex = i;
                currentAudio = Audio(emotions[selectedIndex].trainedSound);
                player.stop();
              });
            },
            child: Opacity(
              opacity: selectedIndex == i ? 1 : 0.5,
              child: Image.asset(
                emotion.image,
                height: hp(8, context),
                width: hp(8, context),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildSoundVisualizer() {
    return Padding(
      padding: EdgeInsets.only(top: hp(10, context), left: wp(10, context)),
      child: Visualizer(
        builder: (BuildContext context, List<int> wave) {
          final _random = new Random();
          wave = List.generate(10000, (i) => _random.nextInt(150));
          return new CustomPaint(
            painter: CircularBarVisualizer(
              waveData: wave,
              height: hp(43, context),
              width: hp(43, context),
              color: Colors.blueAccent[100],
            ),
            child: new Container(),
          );
        },
        id: 1,
      ),
    );
  }
}
