import 'package:emotional_text_to_speech/Models/Sentence.dart';
import 'package:path_provider/path_provider.dart';

import 'package:emotional_text_to_speech/Utilities/Constants.dart';
import 'package:dio/dio.dart';

class SentenceRepo {
  Dio dio = Dio();

  Future<String> sendSentence(Sentence sentence) async {
    final url = Uri.http(Constants.BaseUrl, "/myaudio", sentence.toJson());
    var dir = await getApplicationDocumentsDirectory();

    print(url.toString());

    await dio.download(url.toString(), "${dir.path}/audio2.wav",
        onReceiveProgress: (rec, total) {
      print("Rec: $rec , Total: $total");
    }).catchError((onError) {
      print(onError.toString());
    });

    return "${dir.path}/audio2.wav";
  }
}
