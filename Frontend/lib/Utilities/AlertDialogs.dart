import 'package:flutter/material.dart';

import 'Constants.dart';

void showWarningDialog(String title, String content, BuildContext context) {
  showDialog(
      context: context,
      builder: (_context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(Constants.BorderRadius)),
          title: Row(
            children: <Widget>[
              Icon(
                Icons.warning,
                color: Colors.yellow[700],
              ),
              Text(
                " $title",
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(_context),
              child: Text("Close"),
            ),
          ],
        );
      });
}